#pragma once

struct Vector2 {
	float x,y;
}[[static]];

struct Vector3 {
	float x,y,z;
}[[static]];

struct Vector4 {
	float x,y,z,w;
}[[static]];