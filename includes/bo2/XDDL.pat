#pragma once

struct ddlDef_t {
	s32 version;
	s32 size;
	
	u32 structList_ptr;
	s32 structCount;
	
	u32 enumList_ptr;
	s32 enumCount;
	
	u32 next_ptr;
}[[static]];

struct XDDL {
	u32 name_ptr;
	u32 ddlDef_ptr;
	
	char name[];
	
	ddlDef_t ddlDef;
};