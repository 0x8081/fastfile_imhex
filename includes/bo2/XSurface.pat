#pragma once

struct XSurfaceCollisionAabb {
	u16 mins[3];
	u16 maxs[3];
}[[static]];

struct XSurfaceCollisionNode {
	XSurfaceCollisionAabb aabb;
	u16 childBeginIndex;
	u16 childCount;
}[[static]];

struct XSurfaceCollisionLeaf {
	u16 triangleBeginIndex;
}[[static]];

struct XSurfaceCollisionTree {
	Vector3 trans;
	Vector3 scale;
	
	u32 nodeCount;
	u32 nodes;
	u32 leafCount;
	u32 leafs;
}[[static]];

struct XSurfaceVertexInfo {
	u16 vertCount[4];
	u32 vertsBlend;
	float tensionData;
}[[static]];

struct XSurface {
	u8 tileMode;
	u8 vertListCount;
	
	u16 flags;
	u16 vertexCount;
	u16 triCount;
	u16 baseVertIndex;
	u16 baseTriIndex;
	
	u32 triIndices;
	
	XSurfaceVertexInfo vertInfo;

    u32 verts0;
    u32 vb0;
    u32 rigidWeightsPtr;
    u32 indexBuffer;

    u32 PartBits[5];

    padding[0xC]; 
};