#pragma once

struct ObjAnimMat {
	Vector3 trans;
	Vector4 quat;
	float transWeight;
}[[static]];

struct XRigidVertList {
	u16 boneOffset;
	u16 vertCount;
	u16 triOffset;
	u16 triCount;
	
	u32 collisionTreePtr;
}[[static]];

struct XModelLodInfo {
	float dist;
	u16 numsurfs;
	u16 surfIndex;
	u32 partBits[5];
}[[static]];

struct XModel {
	// XModel Struct in Memory
	u32 namePtr;
	
	u8 numBones;
	u8 numRootBones;
	u8 numSurfs;
	u8 lodRampType;
	
	u32 ptr0;
	u32 ptr1;
	u32 ptr2;
	u32 ptr3;
	
	u32 partClass;
	u32 baseMat;
	u32 surfs;
	u32 materialsPtr;
	
	XModelLodInfo lodInfo[4];
	
	u32 collSurfs;
	u32 numCollSurfs;
	u32 contents;
	
	u32 ptr4;
	
	float radius;
	Vector3 mins;
	Vector3 maxs;
	
	u16 numLods;
	u16 collLod;
	u32 himipInvSqRadii;
	
	u32 memUsage;
	u32 flags;
	
	bool bad;
	padding[3];
	
	u32 physPreset;
	u8 numCollMaps;
	u32 collmaps;
	padding[3];
	
	u32 physConstraints;
	Vector3 lightingOriginOffset;
	float lightingOriginRange;
}[[static]];