#pragma once

struct DB_Header {
	char magic[8];
	u32 version;
}[[static]];

struct DB_AuthSignature {
	char bytes[256];
}[[static]];

struct DB_AuthHeader {
	char magic[8];
	u32 reserved;
	char fastfileName[32];
	DB_AuthSignature singedHash;
}[[static]];

struct DB_Section {
	u32 size;
	u8 data[size];
};